import pygame
#instance des niveaux
class Stage:
	def __init__(self, name, nPlatforms, maxLength):
		self.platformList = []
		self.collisionList = []
		self.name = name
		self.nPlatforms = nPlatforms
		self.maxLength = maxLength
	def getName(self):
		return self.name
	def createPlatform(self, x, y, sizeType):
		if sizeType == "large":
			platform = pygame.Rect(x, y, self.maxLength, 10)
		elif sizeType == "small":
			platform = pygame.Rect(x, y, self.maxLength//2, 5)
		elif sizeType == "tiny":
			platform = pygame.Rect(x, y, self.maxLength//4, 2)
		elif sizeType == "main":
			platform = pygame.Rect(x, y, self.maxLength, 15)	
		else: 
			print("last argument needs one of these str:\nmain, large, small or tiny")
			return
		self.platformList.append(platform)
		self.collisionList.append(platform)
	def draw(self, win):
		for i in range(0, len(self.platformList)):
			pygame.draw.rect(win, (255, 255, 255), self.platformList[i])
	def getCollisions(self):
		return self.collisionList

#liste des stages:		
