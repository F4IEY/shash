from pygame import joystick


class Controller:
	def __init__(self, number):
		joystick.init()
		#on affiche la liste des controleurs disponibles
		for i in range(joystick.get_count()):
			self.scanner = joystick.Joystick(i)
			print(i, ") ", self.scanner.get_name(), "\n")
		self.controller = joystick.Joystick(number) #bien verifier qu'il s'agit du bon!
		self.controller.init()
		print(self.controller.get_name(), " is active!")
	def getInputs(self):
		#renvoie le dictionnaire des touches
		inputs = {}
		#touches utiles: x/y/Cx/Cy/A/B/X/Y/L/R/Z/start/Dpad
		#inputs = [self.controller.get_axis(0), self.controller.get_axis(1), self.controller.get_axis(2), self.controller.get_axis(3), self.controller.get_button(0)]
		inputs["x"] = self.controller.get_axis(0)
		inputs["y"] = self.controller.get_axis(1)
		inputs["Cx"] = self.controller.get_axis(4)
		inputs["Cy"] = self.controller.get_axis(3)
		inputs["A"] = self.controller.get_button(0)
		inputs["B"] = self.controller.get_button(1)
		inputs["X"] = self.controller.get_button(2)
		inputs["Y"] = self.controller.get_button(3)
		inputs["Z"] = self.controller.get_button(5)
		inputs["start"] = self.controller.get_button(7)
		inputs["DpadX"], inputs["DpadY"] = self.controller.get_hat(0) #ne sont pas des bool: fonctionnent comme les axis
		#on utilise les gachettes analogiques comme des boutons (avec dead zones)
		if self.controller.get_axis(2) > 0.2:
			inputs["L"] = True
			inputs["R"] = False #or Button11	
		elif self.controller.get_axis(2) < -0.2:
			inputs["R"] = True
			inputs["L"] = False #or Button10
		else:
			inputs["L"] = False #or Button10
			inputs["R"] = False #or Button11	
		return inputs	